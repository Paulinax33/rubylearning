puts 1 + 4
puts -6 + 13
puts 10 - 6
puts 3 * 4
puts 5 * 5 * 293

#PEMDAS - skrót oznaczający nawiasy, dodawanie, odejmowanie itp
puts (2 + 3) * 5 #nawiasy zmieniają priorytet liczenia
puts 10 / 5
puts 12 / 5 #tutaj jest odcinana liczba po przecinku, pokazuje się tylko liczba całkowita
