print "This random nonsense"
print "will continue on the same line" //pojawi się po sobie bez spacji

puts "Steven Seagal"
p "Steven Seagal" //p method poda nam tekst w cudzysłowie

puts 5 //liczba
puts "5" //fragment tekst, ale wypisuje bez cudzysłowia
p "5" //wywołuje w cudzysłowie
p 5 //wywołuje bez cudzysłowia

puts "Hi there\nline break" //\n tam pojawia się line break
p "Hi there \nline break" //pojawi się w tekście "\n" i nie zrobi line break'a
