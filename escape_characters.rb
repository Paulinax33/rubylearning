puts "some text \nmore text" //teksty będą znajdowały się pod spodem
puts "\tOnce upon a time" //będzie występywał tabulator przed tekstem

puts "Juliet said \"Goodbye\" to Romeo" //Goodbye będzie w cudzysłowie
puts 'Juliet said \'Goodbye\' to Romeo' //Goodbye będzie w pojedynczym cudzysłowie

puts "Juliet said 'Goodbye' to Romeo" //Goodbye będzie w pojedynczym cudzysłowie
puts 'Juliet said "Goodbye" to Romeo' //Goodbye będzie w cudzysłowie
